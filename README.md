# Dell racadm in docker

This repo provides a container with commandline tools provided by Dell to interact with [iDRAC](http://en.community.dell.com/techcenter/systems-management/w/wiki/3205.racadm-command-line-interface-for-drac) for server management.

## 2016-06-13 update

Consider using https://github.com/ajeetraina/Dell-System-Monitoring.git as described http://en.community.dell.com/techcenter/b/techcenter/archive/2016/05/10/monitoring-dell-infrastructure-using-docker-containers-and-microservices


## Notes

- This image is over 300 MB in size while the Dell software tarball is only about 60 MB

- Not sure how long the Dell download link will remain active. If *wget* fails, then you have to go to http://support.dell.com/ to find the current download link for *Dell OpenManage Linux Remote Access Utilities,v8.2* or a later version if available

- CentOS openssl is actually version 1.0+ but API-compatible with version 0.9.8+ required by racadm5, hence the *ln -f* in the Dockerfile

## Usage

- Either pull the image from Docker hub or build it locally like this:

```
docker build --rm -t testimg .
```
- Invoke racadm5 like so:

```
docker run testimg /opt/dell/srvadmin/bin/racadm5 -r {server_name_or_ip} -u {idrac_user} -p {password} help
```